(function ($) {
  $.fn.dynamicInput = function (options) {
    var wrapper = $(this);
    var template;

    function getOptions() {
      var defaults = {
        afterAdd: function (element) {},
        afterDelete: function () {}
      };

      options = $.extend(defaults, options);
    }

    function getTemplate() {
      var diTemplate = wrapper.find('#di-template');
      template = diTemplate.clone().removeClass('#di-template');
      diTemplate.remove();
    }

    function addRow() {
      var epoch = new Date().getTime();
      var newElement = template.clone().addClass('di-item');

      newElement.html(newElement.html().replace(/:index/g, epoch));

      wrapper.find('#di-container').append(newElement);

      options.afterAdd(newElement);
    }

    function deleteRow(element) {
      element.closest('.di-item').remove();

      options.afterDelete();
    }

    function bindButton() {
      wrapper.on('click', '.di-add', function () {
        addRow();
      });

      wrapper.on('click', '.di-delete', function () {
        deleteRow($(this));
      });
    }

    getOptions(options);
    getTemplate();
    bindButton();
  };
})(jQuery);
