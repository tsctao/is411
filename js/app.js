$.fn.select2.defaults.set( "theme", "bootstrap" );

$(document).on('submit', '.confirm-delete', function (event) {
  if (!confirm('Are you sure to delete this item?')) {
    event.preventDefault();
  }
});

$(document).on('click', '#logout-button', function (event) {
  event.preventDefault();
  $('#logout-form').submit();
});

$(document).on('change', '.calc-quantity', function () {
  calc();
});

$(document).on('change', '.calc-price', function () {
  calc();
});

function calc() {
  var prices = $('.calc-price:visible');
  var quantities = $('.calc-quantity:visible');
  var totalAmount = 0;
  var totalQuantity = 0;

  prices.each(function (_index, element) {
    element = $(element);

    var price = element.is('input') ? element.val() : element.text();
    price = parseFloat(price);
    var index = element.data('index');
    var quantity = parseInt(quantities.filter('[data-index="' + index + '"]').val());
    var extPrice = price * quantity;

    totalQuantity += quantity;
    totalAmount += extPrice;

    extPrice = isNaN(extPrice) ? 0 : extPrice.toFixed(2);

    $('.calc-ext[data-index="' + index + '"]').html(extPrice);
  });

  totalAmount = isNaN(totalAmount) ? 0 : totalAmount.toFixed(2);

  $('.calc-total-quantity').html(totalQuantity);
  $('.calc-total-amount').html(totalAmount);
}

calc();
