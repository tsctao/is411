<?php

session_start();

// Define base path of application.
define('BASE_PATH', str_replace('\\', '/', __DIR__));

// Save old input into session
if (isset($_SESSION['redirect']) && $_SESSION['redirect']) {
    unset($_SESSION['redirect']);
} else {
    $_SESSION['old_input'] = $_POST + $_GET;
    unset($_SESSION['errors']);
}

// Load Config
require_once 'config/index.php';

// Load Helper
require_once 'helpers/index.php';

$auth = isset($_SESSION['user']) ? query_find($database, 'user', $_SESSION['user'], 'username') : null;
