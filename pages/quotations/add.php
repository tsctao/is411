<?php

require_once '../../bootstrap.php';
auth_check($auth, 'admin', true);

$title = 'Add Quotation';

$model = [];

$view = 'quotations/add.template.php';
include '../../views/template.php';
