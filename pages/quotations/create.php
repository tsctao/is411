<?php

require_once '../../bootstrap.php';
auth_check($auth, 'admin', true);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $inputs = [
        'supplier_id' => $_POST['supplier_id'],
        'status' => 'open',
        'items' => $_POST['items'],
    ];

    $validation = validate([
        'supplier_id' => ['required'],
        'items' => ['required'],
    ], $inputs);

    if (!$validation) {
        redirect('/pages/quotations/add.php');
    }

    $quotation = $inputs;
    unset($quotation['items']);

    $quotation_id = query_insert($database, 'quotation', $quotation);

    foreach ($inputs['items'] as $item) {
        $item['quotation_id'] = $quotation_id;
        query_insert($database, 'quotation_item', $item);
    }

    flash_set([
        'message' => 'Quotation created.',
        'type' => 'success'
    ]);
}

redirect('/pages/quotations/index.php');
