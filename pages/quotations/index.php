<?php

require_once '../../bootstrap.php';

auth_check($auth, 'admin', true);

$title = 'Quotation List';

$quotations = query_list($database, 'quotation', [
    'sortable' => ['created_at', 'updated_at', 'supplier.name', 'status'],
    'default_sort' => 'updated_at',
    'default_direction' => 'desc',
    'searchable' => ['created_at', 'updated_at', 'supplier.name', 'status'],
    'pagination' => 10,
    'dates' => ['created_at', 'updated_at'],
    'join' => [
        ['supplier', 'id', 'supplier_id', ['name']]
    ],
]);

$view = 'quotations/index.template.php';
include '../../views/template.php';
