<?php

require_once '../../bootstrap.php';

auth_check($auth, 'admin', true);


$title = 'Supplier List';

$suppliers = query_list($database, 'supplier', [
    'sortable' => ['name','address', 'email', 'telephone', 'updated_at'],
    'default_sort' => 'updated_at',
    'default_direction' => 'desc',
    'searchable' => ['name','address', 'email', 'telephone', 'updated_at'],
    'pagination' => 10,
    'dates' => ['updated_at'],
]);

$view = 'suppliers/index.template.php';
include '../../views/template.php';
