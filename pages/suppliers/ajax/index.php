<?php

require_once '../../../bootstrap.php';

$suppliers = query_list($database, 'supplier', [
    'searchable' => ['name'],
    'default_sort' => 'updated_at',
    'default_direction' => 'desc',
]);

echo json_encode($suppliers);
