<?php

require_once '../../bootstrap.php';

auth_check($auth, 'admin', true);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $inputs = [
        'name' => $_POST['name'],
        'address' => $_POST['address'],
        'email' => $_POST['email'],
        'telephone' => $_POST['telephone'],
     
    ];

    $validation = validate([
        'name' => ['required'],
        'address' => ['required'],
        'email' => ['required'],
        'telephone' => ['required'],
     
        
    ], $inputs);

    if (!$validation) {
        redirect('/pages/suppliers/edit.php', ['id' => $_GET['id']]);
    }

    query_update($database, 'supplier', $_GET['id'], $inputs);

    flash_set([
        'message' => 'Supplier updated.',
        'type' => 'success'
    ]);
}

redirect('/pages/suppliers/index.php');
