<?php

require_once '../../bootstrap.php';

auth_check($auth, 'admin', true);

$title = 'Edit Supplier';

$model = query_find($database, 'supplier', query_string('id'));

$view = 'suppliers/edit.template.php';
include '../../views/template.php';
