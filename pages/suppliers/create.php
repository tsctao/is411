<?php

require_once '../../bootstrap.php';

auth_check($auth, 'admin', true);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $inputs = [
        'name' => $_POST['name'],
        'address' => $_POST['address'],
        'email' => $_POST['email'],
        'telephone' => $_POST['telephone'],    
    ];

    $validation = validate([
        'name' => ['required'],
        'address' => ['required'],
        'email' => ['required'],
        'telephone' => ['required'],
        
    ], $inputs);

    if (!$validation) {
        redirect('/pages/suppliers/add.php');
    }

    query_insert($database, 'supplier', $inputs);

    flash_set([
        'message' => 'Supplier created.',
        'type' => 'success'
    ]);
}

redirect('/pages/suppliers/index.php');
