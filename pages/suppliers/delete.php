<?php

require_once '../../bootstrap.php';

auth_check($auth, 'admin', true);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    query_delete($database, 'supplier', $_GET['id']);
}

redirect('/pages/suppliers/index.php');