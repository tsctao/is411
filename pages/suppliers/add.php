<?php

require_once '../../bootstrap.php';

auth_check($auth, 'admin', true);

$title = 'Add Supplier';

$model = [];

$view = 'suppliers/add.template.php';
include '../../views/template.php';
