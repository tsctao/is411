<?php

require_once '../../bootstrap.php';
auth_check($auth, 'admin', true);
$title = 'Add Purchase Order';

if (is_null(query_string('quotation_id'))) {
    redirect('/pages/quotations/index.php');
}

$quotation = query_find($database, 'quotation', query_string('quotation_id'));
if ($quotation['status'] != 'open') {
    redirect('/pages/purchase_orders/index.php');
}
$supplier = query_find($database, 'supplier', $quotation['supplier_id']);
$quotation_items = query_find($database, 'quotation_item', $quotation['id'], 'quotation_id', ['list' => true]);
$products = query_find($database, 'product', array_map(function ($item) {
    return $item['product_id'];
}, $quotation_items), 'id', ['list' => true]);

$total_amount = 0;
$total_quantity = 0;

foreach ($quotation_items as $item) {
    $total_amount += $item['price'] * $item['quantity'];
    $total_quantity += $item['quantity'];
}

$view = 'purchase_orders/add.template.php';
include '../../views/template.php';
