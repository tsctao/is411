<?php

require_once '../../bootstrap.php';
auth_check($auth, 'admin', true);
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['quotation_id'])) {
    $quotation = query_find($database, 'quotation', $_POST['quotation_id']);
    if ($quotation['status'] != 'open') {
        redirect('/pages/purchase_orders/index.php');
    }

    query_insert($database, 'purchase_order', [
        'quotation_id' => $_POST['quotation_id'],
        'status' => 'open',
    ]);

    query_update($database, 'quotation', $_POST['quotation_id'], [
        'status' => 'complete'
    ]);

    flash_set([
        'message' => 'Purchase order created.',
        'type' => 'success'
    ]);
}

redirect('/pages/purchase_orders/index.php');
