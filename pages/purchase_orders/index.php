<?php

require_once '../../bootstrap.php';

auth_check($auth, 'admin', true);

$title = 'Purchase Order List';

$purchase_orders = query_list($database, 'purchase_order', [
    'sortable' => ['created_at', 'updated_at', 'supplier.name', 'status'],
    'default_sort' => 'updated_at',
    'default_direction' => 'desc',
    'searchable' => ['created_at', 'updated_at', 'supplier.name', 'status'],
    'pagination' => 10,
    'dates' => ['created_at', 'updated_at'],
    'join' => [
        ['quotation', 'id', 'quotation_id', ['supplier_id']],
        ['supplier', 'id', 'quotation.supplier_id', ['name']]
    ],
]);

$view = 'purchase_orders/index.template.php';
include '../../views/template.php';
