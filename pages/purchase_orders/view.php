<?php

require_once '../../bootstrap.php';
auth_check($auth, 'admin', true);
$title = 'View Purchase Order #' . query_string('id');

$purchase_order = query_find($database, 'purchase_order', query_string('id'));
$quotation = query_find($database, 'quotation', $purchase_order['quotation_id']);
$supplier = query_find($database, 'supplier', $quotation['supplier_id']);
$quotation_items = query_find($database, 'quotation_item', $quotation['id'], 'quotation_id', ['list' => true]);
$products = query_find($database, 'product', array_map(function ($item) {
    return $item['product_id'];
}, $quotation_items), 'id', ['list' => true]);
$bill_of_lading = query_find($database, 'bill_of_lading', query_string('id'), 'purchase_order_id');

$total_amount = 0;
$total_quantity = 0;

foreach ($quotation_items as $item) {
    $total_amount += $item['price'] * $item['quantity'];
    $total_quantity += $item['quantity'];
}

$view = 'purchase_orders/view.template.php';
include '../../views/template.php';
