<?php

require_once '../../bootstrap.php';

auth_check($auth, ['admin','user'], true); 

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $sales_order = query_find($database, 'sales_order', query_string('id'));
    if ($sales_order['status'] != 'open') {
        redirect('/pages/sales_orders/index.php');
    }

    query_update($database, 'sales_order', query_string('id'), [
        'status' => 'cancel'
    ]);

    $sales_order_items = query_find($database, 'sales_order_item', query_string('id'), 'sales_order_id', ['list' => true]);

    $products = query_find($database, 'product', array_map(function ($item) {
        return $item['product_id'];
    }, $sales_order_items), 'id', ['list' => true]);

    foreach ($sales_order_items as $item) {
        $product = array_where($products, 'id', $item['product_id']);
        $quantity = $product['quantity'] + $item['quantity'];
        query_update($database, 'product', $product['id'], [
            'quantity' => $quantity
        ]);
    }
}

redirect('/pages/sales_orders/index.php');
