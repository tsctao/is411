<?php

require_once '../../bootstrap.php';

auth_check($auth, ['admin','user'], true); 

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $sales_order = query_find($database, 'sales_order', query_string('id'));
    if ($sales_order['status'] != 'open') {
        redirect('/pages/sales_orders/index.php');
    }

    $inputs = [
        'customer_id' => $_POST['customer_id'],
        'items' => $_POST['items'],
    ];

    $validation = validate([
        'customer_id' => ['required'],
        'items' => ['required'],
    ], $inputs);

    if (!$validation) {
        redirect('/pages/sales_orders/add.php');
    }

    $sales_order = $inputs;
    unset($sales_order['items']);

    query_update($database, 'sales_order', query_string('id'), $sales_order);

    $sales_order_items = query_find($database, 'sales_order_item', query_string('id'), 'sales_order_id', ['list' => true]);

    $products = query_find($database, 'product', array_map(function ($item) {
        return $item['product_id'];
    }, $sales_order_items), 'id', ['list' => true]);

    foreach ($sales_order_items as $item) {
        query_delete($database, 'sales_order_item', $item['id']);

        $product = array_where($products, 'id', $item['product_id']);
        $quantity = $product['quantity'] + $item['quantity'];
        query_update($database, 'product', $product['id'], [
            'quantity' => $quantity
        ]);
    }

    $products = query_find($database, 'product', array_map(function ($item) {
        return $item['product_id'];
    }, $inputs['items']), 'id', ['list' => true]);

    foreach ($inputs['items'] as $item) {
        $product = array_where($products, 'id', $item['product_id']);

        $item['sales_order_id'] = query_string('id');
        $item['price'] = $product['price'];
        query_insert($database, 'sales_order_item', $item);

        $quantity = $product['quantity'] - $item['quantity'];
        query_update($database, 'product', $product['id'], [
            'quantity' => $quantity
        ]);
    }

    flash_set([
        'message' => 'Sales order updated.',
        'type' => 'success'
    ]);
}

redirect('/pages/sales_orders/index.php');
