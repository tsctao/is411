<?php

require_once '../../bootstrap.php';

auth_check($auth, ['admin','user'], true); 

$title = 'Add Sales Order';

$sales_order = [];
$sales_order_items = [];

$view = 'sales_orders/add.template.php';
include '../../views/template.php';
