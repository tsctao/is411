<?php

require_once '../../bootstrap.php';

auth_check($auth, ['admin','user'], true); 


$title = 'Sales Order List';

$sales_orders = query_list($database, 'sales_order', [
    'sortable' => ['created_at', 'updated_at', 'customer.name', 'status'],
    'default_sort' => 'updated_at',
    'default_direction' => 'desc',
    'searchable' => ['created_at', 'updated_at', 'customer.name', 'status'],
    'pagination' => 10,
    'dates' => ['created_at', 'updated_at'],
    'join' => [
        ['customer', 'id', 'customer_id', ['name']]
    ],
]);

$view = 'sales_orders/index.template.php';
include '../../views/template.php';
