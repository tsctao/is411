<?php

require_once '../../bootstrap.php';

auth_check($auth, ['admin','user'], true); 

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $inputs = [
        'customer_id' => $_POST['customer_id'],
        'status' => 'open',
        'items' => $_POST['items'],
    ];

    $validation = validate([
        'customer_id' => ['required'],
        'items' => ['required'],
    ], $inputs);

    if (!$validation) {
        redirect('/pages/sales_orders/add.php');
    }

    $sales_order = $inputs;
    unset($sales_order['items']);

    $sales_order_id = query_insert($database, 'sales_order', $sales_order);

    $products = query_find($database, 'product', array_map(function ($item) {
        return $item['product_id'];
    }, $inputs['items']), 'id', ['list' => true]);

    foreach ($inputs['items'] as $item) {
        $product = array_where($products, 'id', $item['product_id']);

        $item['sales_order_id'] = $sales_order_id;
        $item['price'] = $product['price'];
        query_insert($database, 'sales_order_item', $item);

        $quantity = $product['quantity'] - $item['quantity'];
        query_update($database, 'product', $product['id'], [
            'quantity' => $quantity
        ]);
    }

    flash_set([
        'message' => 'Sales order created.',
        'type' => 'success'
    ]);
}

redirect('/pages/sales_orders/index.php');
