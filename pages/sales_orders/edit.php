<?php

require_once '../../bootstrap.php';

auth_check($auth, ['admin','user'], true); 

$title = 'Edit Sales Order';

$sales_order = query_find($database, 'sales_order', query_string('id'));
if ($sales_order['status'] != 'open') {
    redirect('/pages/sales_orders/index.php');
}
$customer = query_find($database, 'customer', $sales_order['customer_id']);
$sales_order_items = query_find($database, 'sales_order_item', $sales_order['id'], 'sales_order_id', ['list' => true]);
$products = query_find($database, 'product', array_map(function ($item) {
    return $item['product_id'];
}, $sales_order_items), 'id', ['list' => true]);

$view = 'sales_orders/edit.template.php';
include '../../views/template.php';
