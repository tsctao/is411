<?php

require_once '../../../bootstrap.php';

$products = query_list($database, 'product', [
    'searchable' => ['name'],
]);

echo json_encode($products);
