<?php

require_once '../../bootstrap.php';
auth_check($auth, 'admin', true);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $inputs = [
        'name' => $_POST['name'],
        'brand' => $_POST['brand'],
        'description' => $_POST['description'],
        'price' => $_POST['price']
    ];

    $validation = validate([
        'name' => ['required'],
        'brand' => ['required'],
        'price' => ['required'],
    ], $inputs);

    if (!$validation) {
        redirect('/pages/products/add.php');
    }

    query_insert($database, 'product', $inputs);

    flash_set([
        'message' => 'Product created.',
        'type' => 'success'
    ]);
}

redirect('/pages/products/index.php');
