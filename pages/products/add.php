<?php

require_once '../../bootstrap.php';
auth_check($auth, 'admin', true);
$title = 'Add Product';

$model = [];

$view = 'products/add.template.php';
include '../../views/template.php';
