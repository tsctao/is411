<?php

require_once '../../bootstrap.php';

auth_check($auth, 'admin', true);

$title = 'Product List';

$products = query_list($database, 'product', [
    'sortable' => ['name', 'brand', 'quantity', 'price', 'updated_at'],
    'default_sort' => 'updated_at',
    'default_direction' => 'desc',
    'searchable' => ['name', 'brand', 'quantity', 'price', 'updated_at'],
    'pagination' => 10,
    'dates' => ['updated_at'],
]);

$view = 'products/index.template.php';
include '../../views/template.php';
