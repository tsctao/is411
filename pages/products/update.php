<?php

require_once '../../bootstrap.php';
auth_check($auth, 'admin', true);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $inputs = [
        'name' => $_POST['name'],
        'brand' => $_POST['brand'],
        'description' => $_POST['description'],
        'price' => $_POST['price']
    ];

    $validation = validate([
        'name' => ['required'],
        'brand' => ['required'],
        'price' => ['required', 'decimal'],
    ], $inputs);

    if (!$validation) {
        redirect('/pages/products/edit.php', ['id' => $_GET['id']]);
    }

    query_update($database, 'product', $_GET['id'], $inputs);

    flash_set([
        'message' => 'Product updated.',
        'type' => 'success'
    ]);
}

redirect('/pages/products/index.php');
