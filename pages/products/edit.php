<?php

require_once '../../bootstrap.php';
auth_check($auth, 'admin', true);
$title = 'Edit Product';

$model = query_find($database, 'product', query_string('id'));

$view = 'products/edit.template.php';
include '../../views/template.php';
