<?php

require_once '../../bootstrap.php';
auth_check($auth, 'admin', true);
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['purchase_order_id'])) {
    $purchase_order = query_find($database, 'purchase_order', $_POST['purchase_order_id']);
    if ($purchase_order['status'] != 'open') {
        redirect('/pages/bills_of_lading/index.php');
    }

    $inputs = [
        'purchase_order_id' => $_POST['purchase_order_id'],
        'items' => $_POST['items'],
    ];

    $validation = validate([
        'items' => ['required'],
    ], $inputs);

    if (!$validation) {
        redirect('/pages/quotations/add.php');
    }

    $bill_of_lading = $inputs;
    unset($bill_of_lading['items']);

    $bill_of_lading_id = query_insert($database, 'bill_of_lading', $bill_of_lading);

    query_update($database, 'purchase_order', $_POST['purchase_order_id'], [
        'status' => 'complete'
    ]);

    $products = query_find($database, 'product', array_map(function ($item) {
        return $item['product_id'];
    }, $inputs['items']), 'id', ['list' => true]);

    foreach ($inputs['items'] as $item) {
        $item['bill_of_lading_id'] = $bill_of_lading_id;
        query_insert($database, 'bill_of_lading_item', $item);

        $product = array_where($products, 'id', $item['product_id']);
        $quantity = $product['quantity'] + $item['quantity'];

        query_update($database, 'product', $product['id'], [
            'quantity' => $quantity
        ]);
    }

    flash_set([
        'message' => 'Bill of lading created.',
        'type' => 'success'
    ]);
}

redirect('/pages/bills_of_lading/index.php');
