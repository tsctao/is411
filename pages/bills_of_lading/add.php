<?php

require_once '../../bootstrap.php';
auth_check($auth, 'admin', true);
$title = 'Add Bill of Lading';

if (is_null(query_string('purchase_order_id'))) {
    redirect('/pages/purchase_orders/index.php');
}

$purchase_order = query_find($database, 'purchase_order', query_string('purchase_order_id'));
if ($purchase_order['status'] != 'open') {
    redirect('/pages/bills_of_lading/index.php');
}
$quotation = query_find($database, 'quotation', $purchase_order['quotation_id']);
$supplier = query_find($database, 'supplier', $quotation['supplier_id']);
$quotation_items = query_find($database, 'quotation_item', $quotation['id'], 'quotation_id', ['list' => true]);
$products = query_find($database, 'product', array_map(function ($item) {
    return $item['product_id'];
}, $quotation_items), 'id', ['list' => true]);

$total_amount = 0;
$total_quantity = 0;

foreach ($quotation_items as $item) {
    $total_amount += $item['price'] * $item['quantity'];
    $total_quantity += $item['quantity'];
}

$view = 'bills_of_lading/add.template.php';
include '../../views/template.php';
