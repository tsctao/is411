<?php

require_once '../../bootstrap.php';
auth_check($auth, 'admin', true);
$title = 'View Bill of Lading #' . query_string('id');

$bill_of_lading = query_find($database, 'bill_of_lading', query_string('id'));
$bill_of_lading_items = query_find($database, 'bill_of_lading_item', $bill_of_lading['id'], 'bill_of_lading_id', ['list' => true]);
$products = query_find($database, 'product', array_map(function ($item) {
    return $item['product_id'];
}, $bill_of_lading_items), 'id', ['list' => true]);
$purchase_order = query_find($database, 'purchase_order', $bill_of_lading['purchase_order_id']);
$quotation = query_find($database, 'quotation', $purchase_order['quotation_id']);
$supplier = query_find($database, 'supplier', $quotation['supplier_id']);

$total_amount = 0;
$total_quantity = 0;

foreach ($bill_of_lading_items as $item) {
    $total_amount += $item['price'] * $item['quantity'];
    $total_quantity += $item['quantity'];
}

$view = 'bills_of_lading/view.template.php';
include '../../views/template.php';
