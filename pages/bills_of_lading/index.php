<?php

require_once '../../bootstrap.php';

auth_check($auth, 'admin', true);

$title = 'Bill of Lading List';

$bills_of_lading = query_list($database, 'bill_of_lading', [
    'sortable' => ['created_at', 'supplier.name'],
    'default_sort' => 'created_at',
    'default_direction' => 'desc',
    'searchable' => ['created_at', 'supplier.name'],
    'pagination' => 10,
    'dates' => ['created_at'],
    'join' => [
        ['purchase_order', 'id', 'purchase_order_id', ['quotation_id']],
        ['quotation', 'id', 'purchase_order.quotation_id', ['supplier_id']],
        ['supplier', 'id', 'quotation.supplier_id', ['name']]
    ],
]);

$view = 'bills_of_lading/index.template.php';
include '../../views/template.php';
