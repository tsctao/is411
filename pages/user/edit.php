<?php

require_once '../../bootstrap.php';

auth_check($auth, 'admin', true);

$title = 'Edit User';

$model = query_find($database, 'user', query_string('id'));

$view = 'user/edit.template.php';
include '../../views/template.php';
