<?php

require_once '../../bootstrap.php';

auth_check($auth, 'admin', true);

$title = 'User list';


$users = query_list($database, 'user', [
    'sortable' => ['name', 'address', 'username', 'role', 'updated_at', 'created_at'],
    'default_sort' => 'updated_at',
    'default_direction' => 'desc',
    'searchable' => ['name', 'address', 'username', 'role', 'updated_at', 'created_at'],
    'pagination' => 10,
    'dates' => ['updated_at','created_at'],	
]);

$view = 'user/index.template.php';
include '../../views/template.php';
