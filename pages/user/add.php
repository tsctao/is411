<?php

require_once '../../bootstrap.php';

auth_check($auth, 'admin', true);

$title = 'Add User';

$model = [];

$view = 'user/add.template.php';
include '../../views/template.php';
