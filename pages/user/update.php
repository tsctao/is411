<?php

require_once '../../bootstrap.php';

auth_check($auth, 'admin', true);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $inputs = [
        'name' => $_POST['name'],
        'address' => $_POST['address'],
        'username' => $_POST['username'],
        'password' => $_POST['password'],
		'role' => $_POST['role'],
    ];

    $validation = validate([
       'name' => ['required'],
        'address' => ['required'],
        'username' => ['required'],
		'role' => ['required'],
    ], $inputs);

    if(empty($inputs['password']))
    {
    unset($inputs['password']);
    }else{
    $inputs['password'] = password_hash($inputs['password'], PASSWORD_BCRYPT);
    }
    if (!$validation) {
        redirect('/pages/user/edit.php', ['id' => $_GET['id']]);
    }

    query_update($database, 'user', $_GET['id'], $inputs);

    flash_set([
        'message' => 'User profile was updated.',
        'type' => 'success'
    ]);
}

redirect('/pages/user/index.php');
