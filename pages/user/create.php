<?php

require_once '../../bootstrap.php';

auth_check($auth, 'admin', true);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $inputs = [
        'name' => $_POST['name'],
        'address' => $_POST['address'],
        'username' => $_POST['username'],
        'password' => $_POST['password'],
		'role' => $_POST['role'],
	 
    ];

    $validation = validate([
        'name' => ['required'],
        'address' => ['required'],
        'username' => ['required'],
		'password' => ['required'],
		'role' => ['required'],
	 
    ], $inputs);

    $inputs['password'] = password_hash($inputs['password'], PASSWORD_BCRYPT);

    if (!$validation) {
        redirect('/pages/user/add.php');
    }

    query_insert($database, 'user', $inputs);

    flash_set([
        'message' => 'User added.',
        'type' => 'success'
    ]);
}

redirect('/pages/user/index.php');
