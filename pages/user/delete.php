<?php

require_once '../../bootstrap.php';

auth_check($auth, 'admin', true);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    query_delete($database, 'user', $_GET['id']);
}

redirect('/pages/user/index.php');
