<?php

require_once '../../bootstrap.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST' && auth_is_login($auth)) {
    auth_unset();

    flash_set([
        'message' => 'Logout success.',
        'type' => 'success'
    ]);
}

redirect('/index.php');
