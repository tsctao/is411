<?php

require_once '../../bootstrap.php';

if (auth_is_login($auth)) {
    redirect('/index.php');
}

$title = 'Login';

$view = 'auth/login.template.php';
include '../../views/template.php';
