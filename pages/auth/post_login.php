<?php

require_once '../../bootstrap.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST' && !auth_is_login($auth)) {
    $user = query_find($database, 'user', $_POST['username'], 'username');

    if (!password_verify($_POST['password'], $user['password'])) {
        flash_set([
            'message' => 'Username or password is incorrect',
            'type' => 'danger'
        ]);

        redirect('/pages/auth/login.php');
    }


    auth_set($user['username']);

    flash_set([
        'message' => 'Login success.',
        'type' => 'success'
    ]);

    redirect('/index.php');
}

redirect('/pages/auth/login.php');
