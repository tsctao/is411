<?php

require_once '../../bootstrap.php';

if (!auth_is_login($auth)) {
    redirect('/index.php');
}

$title = 'Change Password';

$view = 'auth/change_password.template.php';
include '../../views/template.php';
