<?php

require_once '../../bootstrap.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST' && auth_is_login($auth)) {
    if (!password_verify($_POST['old_password'], $auth['password'])) {
        $_SESSION['errors']['old_password'][] = 'Old password is incorrect';

        redirect('/pages/auth/change_password.php');
    }

    query_update($database, 'user', $auth['id'], ['password' => password_hash($_POST['password'], PASSWORD_BCRYPT)]);

    flash_set([
        'message' => 'Change password success.',
        'type' => 'success'
    ]);

    redirect('/index.php');
}

redirect('/pages/auth/change_password.php');
