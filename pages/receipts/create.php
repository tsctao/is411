<?php

require_once '../../bootstrap.php';
auth_check($auth, ['admin','user'], true); 

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['sales_order_id'])) {
    $sales_order = query_find($database, 'sales_order', $_POST['sales_order_id']);
    if ($sales_order['status'] != 'open') {
        redirect('/pages/receipts/index.php');
    }

    $inputs = [
        'sales_order_id' => $_POST['sales_order_id'],
        'amount' => $_POST['amount']
    ];

    $validation = validate([
        'amount' => ['required'],
    ], $inputs);

    if (!$validation) {
        redirect('/pages/receipts/add.php');
    }

    query_insert($database, 'receipt', $inputs);

    query_update($database, 'sales_order', $_POST['sales_order_id'], [
        'status' => 'complete'
    ]);

    flash_set([
        'message' => 'Receipt created.',
        'type' => 'success'
    ]);
}

redirect('/pages/receipts/index.php');
