<?php

require_once '../../bootstrap.php';

auth_check($auth, ['admin','user'], true); 

$title = 'View Receipt #' . query_string('id');

$receipt = query_find($database, 'receipt', query_string('id'));
$sales_order = query_find($database, 'sales_order', $receipt['sales_order_id']);
$customer = query_find($database, 'customer', $sales_order['customer_id']);
$sales_order_items = query_find($database, 'sales_order_item', $sales_order['id'], 'sales_order_id', ['list' => true]);
$products = query_find($database, 'product', array_map(function ($item) {
    return $item['product_id'];
}, $sales_order_items), 'id', ['list' => true]);

$total_amount = 0;
$total_quantity = 0;

foreach ($sales_order_items as $item) {
    $total_amount += $item['price'] * $item['quantity'];
    $total_quantity += $item['quantity'];
}

$view = 'receipts/view.template.php';
include '../../views/template.php';
