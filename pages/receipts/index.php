<?php

require_once '../../bootstrap.php';

auth_check($auth, ['admin','user'], true); 

$title = 'Receipt List';

$receipts = query_list($database, 'receipt', [
    'sortable' => ['created_at', 'customer.name'],
    'default_sort' => 'created_at',
    'default_direction' => 'desc',
    'searchable' => ['created_at', 'customer.name'],
    'pagination' => 10,
    'dates' => ['created_at'],
    'join' => [
        ['sales_order', 'id', 'sales_order_id', ['customer_id']],
        ['customer', 'id', 'sales_order.customer_id', ['name']]
    ],
]);

$view = 'receipts/index.template.php';
include '../../views/template.php';
