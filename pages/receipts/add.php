<?php

require_once '../../bootstrap.php';
auth_check($auth, ['admin','user'], true); 

$title = 'Add Receipt';

if (is_null(query_string('sales_order_id'))) {
    redirect('/pages/sales_orders/index.php');
}

$sales_order = query_find($database, 'sales_order', query_string('sales_order_id'));
if ($sales_order['status'] != 'open') {
    redirect('/pages/receipts/index.php');
}
$customer = query_find($database, 'customer', $sales_order['customer_id']);
$sales_order_items = query_find($database, 'sales_order_item', $sales_order['id'], 'sales_order_id', ['list' => true]);
$products = query_find($database, 'product', array_map(function ($item) {
    return $item['product_id'];
}, $sales_order_items), 'id', ['list' => true]);

$total_amount = 0;
$total_quantity = 0;

foreach ($sales_order_items as $item) {
    $total_amount += $item['price'] * $item['quantity'];
    $total_quantity += $item['quantity'];
}

$view = 'receipts/add.template.php';
include '../../views/template.php';
