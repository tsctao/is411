<?php

require_once '../../bootstrap.php';
auth_check($auth, 'admin', true);
$title = 'Edit Customer';

$model = query_find($database, 'customer', query_string('id'));

$view = 'customers/edit.template.php';
include '../../views/template.php';
