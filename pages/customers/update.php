<?php

require_once '../../bootstrap.php';
auth_check($auth, 'admin', true);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $inputs = [
        'name' => $_POST['name'],
        'address' => $_POST['address'],
        'email' => $_POST['email'],
        'line' => $_POST['line'],
        'telephone' => $_POST['telephone']
    ];

    $validation = validate([
        'name' => ['required'],
        'address' => ['required'],
        'email' => ['required'],
        'line' => ['required'],
        'telephone' => ['required']
    ], $inputs);

    if (!$validation) {
        redirect('/pages/customers/edit.php', ['id' => $_GET['id']]);
    }

    query_update($database, 'customer', $_GET['id'], $inputs);

    flash_set([
        'message' => 'Customer updated.',
        'type' => 'success'
    ]);
}

redirect('/pages/customers/index.php');
