<?php

require_once '../../../bootstrap.php';

$customer = query_list($database, 'customer', [
    'searchable' => ['name'],
    'default_sort' => 'updated_at',
    'default_direction' => 'desc',
]);

echo json_encode($customer);
