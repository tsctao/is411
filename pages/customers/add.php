<?php

require_once '../../bootstrap.php';
auth_check($auth, 'admin', true);
$title = 'Add Customer';

$model = [];

$view = 'customers/add.template.php';
include '../../views/template.php';
