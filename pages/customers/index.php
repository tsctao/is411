<?php

require_once '../../bootstrap.php';

auth_check($auth, 'admin', true);

$title = 'Customer List';

$customers = query_list($database, 'customer', [
    'sortable' => ['name','address', 'email', 'line', 'telephone', 'updated_at'],
    'default_sort' => 'updated_at',
    'default_direction' => 'desc',
    'searchable' => ['name','address', 'email', 'line', 'telephone', 'updated_at'],
    'pagination' => 10,
    'dates' => ['updated_at'],
]);

$view = 'customers/index.template.php';
include '../../views/template.php';
