<?php

require_once '../../bootstrap.php';
auth_check($auth, 'admin', true);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $inputs = [
        'name' => $_POST['name'],
        'address' => $_POST['address'],
        'email' => $_POST['email'],
        'line' => $_POST['line'],
        'telephone' => $_POST['telephone']
    ];

    $validation = validate([
        'name' => ['required'],
        'address' => ['required'],
        'email' => ['required'],
        'line' => ['required'],
        'telephone' => ['required', 'pattern' => '/^[0-9].{8,}$/']
    ], $inputs);

    if (!$validation) {
        redirect('/pages/customers/add.php');
    }

    query_insert($database, 'customer', $inputs);

    flash_set([
        'message' => 'Customer created.',
        'type' => 'success'
    ]);
}

redirect('/pages/customers/index.php');
