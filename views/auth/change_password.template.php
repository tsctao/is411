<h1><?= $title ?></h1>

<?=
html_breadcrumb([
    'Change Password' => ''
])
?>

<form id="change-password-form" action="<?= url('/pages/auth/post_change_password.php') ?>" method="POST" class="form-horizontal">
    <div class="form-group <?= html_error_class('old_password') ?>">
        <label class="col-sm-2 control-label">Password</label>
        <div class="col-sm-10">
            <input type="password" name="old_password" class="form-control" placeholder="Old Password" required>
            <?= html_error_block('old_password') ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">New Password</label>
        <div class="col-sm-10">
            <input type="password" name="password" class="form-control" placeholder="New Password" required>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Confirm New Password</label>
        <div class="col-sm-10">
            <input type="password" name="confirm_password" class="form-control" placeholder="Confirm New Password" required>
            <span class="help-block" style="display: none;">Confirm password does not match</span>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
        </div>
    </div>
</form>

<?php ob_start(); ?>
<script>
  $('input[name="password"], input[name="confirm_password"]').on('keyup', function () {
    var password = $('input[name="password"]');
    var confirmation = $('input[name="confirm_password"]');

    if (confirmation.val() !== '' && password.val() !== confirmation.val()) {
      confirmation.closest('.form-group').addClass('has-error');
      confirmation.next('.help-block').show();
      $('#change-password-form').off('submit').on('submit', function () {
        return false;
      });
    } else {
      confirmation.closest('.form-group').removeClass('has-error');
      confirmation.next('.help-block').hide();
      $('#change-password-form').off('submit').on('submit', function () {
        return true;
      });
    }
  });
</script>
<?php $javascript = ob_get_clean() ?>
