<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= $title ?> | <?= $config['app_name'] ?></title>

    <link href="<?= url('/css/vendors/bootstrap/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= url('/css/vendors/select2/select2.min.css') ?>" rel="stylesheet">
    <link href="<?= url('/css/vendors/select2/select2-bootstrap.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?= url('/css/app.css') ?>">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= url() ?>"><?= $config['app_name'] ?></a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <?php if (auth_check($auth, 'admin')): ?>
                        <li><a href="<?= url('/pages/user/index.php') ?>">User</a></li>
                    <?php endif; ?>
                    <?php if (auth_check($auth, 'admin')): ?>
                        <li><a href="<?= url('/pages/products/index.php') ?>">Inventory</a></li>
                    <?php endif; ?>
                    <?php if (auth_check($auth, 'admin')): ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Purchase <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= url('/pages/suppliers/index.php') ?>">Supplier</a></li>
                                <li><a href="<?= url('/pages/quotations/index.php') ?>">Quotation</a></li>
                                <li><a href="<?= url('/pages/purchase_orders/index.php') ?>">Purchase Order</a></li>
                                <li><a href="<?= url('/pages/bills_of_lading/index.php') ?>">Bill of Lading</a></li>
                            </ul>
                        </li>
                    <?php endif; ?>
                    <?php if (auth_check($auth, ['admin', 'user'])): ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sale <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php if (auth_check($auth, 'admin')): ?>
                                <li><a href="<?= url('/pages/customers/index.php') ?>">Customer</a></li>
                            <?php endif; ?>
                            <li><a href="<?= url('/pages/sales_orders/index.php') ?>">Sale Order</a></li>
                            <li><a href="<?= url('/pages/receipts/index.php') ?>">Receipt</a></li>
                        </ul>
                    </li>
                    <?php endif; ?>
                    <li><a href="<?= url('/pages/about/index.php') ?>">About Us</a></li>
                    <li><a href="<?= url('/pages/contact/index.php') ?>">Contact Us</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php if (auth_is_login($auth)): ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?= $auth['name'] ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= url('/pages/auth/change_password.php') ?>">Change Password</a></li>
                            </ul>
                        </li>
                        <li>
                            <form id="logout-form" action="<?= url('/pages/auth/post_logout.php') ?>" method="POST">
                            </form>
                            <a id="logout-button" href="#">Logout</a>
                        </li>
                    <?php else: ?>
                        <li><a href="<?= url('/pages/auth/login.php') ?>">Login</a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <?php if ($flash = flash_get()): ?>
            <div class="alert alert-<?= $flash['type'] ?>">
                <?= $flash['message'] ?>
            </div>
        <?php endif; ?>

        <?php include $view ?>
    </div>

    <footer class="footer navbar-fixed-bottom">
        <div class="container">
            <span class="text-muted">Thammasat University</span>
        </div>
    </footer>

    <script src="<?= url('/js/vendors/jquery/jquery-3.1.1.min.js') ?>"></script>
    <script src="<?= url('/js/vendors/bootstrap/bootstrap.min.js') ?>"></script>
    <script src="<?= url('/js/vendors/select2/select2.min.js') ?>"></script>
    <script src="<?= url('/js/dynamicInput.js') ?>"></script>
    <script src="<?= url('/js/app.js') ?>"></script>
    <?= isset($javascript) ? $javascript : '' ?>
</body>
</html>
