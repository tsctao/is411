<h1><?= $title ?></h1>

<?=
html_breadcrumb([
    'Customer' => '/pages/customers/index.php',
    'Edit Customer' => ''
])
?>

<form class="form-horizontal" method="POST" action="<?= url('/pages/customers/update.php', ['id' => $model['id']]) ?>">
    <?php include '_form.template.php' ?>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
            <a href="<?= url('/pages/customers/index.php') ?>" class="btn btn-default">Cancel</a>
        </div>
    </div>
</form>
