<h1><?= $title ?></h1>

<?=
    html_breadcrumb([
        'Product' => '',
    ])
?>

<div class="row toolbar">
    <div class="col-md-6">
        <a href="<?= url('/pages/products/add.php') ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add Product</a>
    </div>
    <div class="col-md-6">
        <form>
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search Product" value="<?= query_string('search') ?>">
                <span class="input-group-btn">
                    <a href="<?= current_url(true) ?>" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span></a>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
                </span>
            </div>
        </form>
    </div>
</div>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Name <?= html_sort('name') ?></th>
        <th>Brand <?= html_sort('brand') ?></th>
        <th>Quantity <?= html_sort('quantity') ?></th>
        <th>Price <?= html_sort('price') ?></th>
        <th>Updated at <?= html_sort('updated_at') ?></th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($products as $product): ?>
        <tr>
            <td><?= $product['name'] ?></td>
            <td><?= $product['brand'] ?></td>
            <td><?= $product['quantity'] ?></td>
            <td><?= $product['price'] ?></td>
            <td><?= $product['updated_at'] ?></td>
            <td>
                <a href="<?= url('/pages/products/edit.php', ['id' => $product['id']]) ?>" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
                <form method="POST" action="<?= url('/pages/products/delete.php', ['id' => $product['id']]) ?>" class="delete-form confirm-delete">
                    <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?= html_pagination() ?>
