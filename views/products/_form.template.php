<div class="form-group <?= html_error_class('name') ?>">
    <label class="col-sm-2 control-label">Name</label>
    <div class="col-sm-10">
        <input type="text" name="name" class="form-control" placeholder="Name" value="<?= input('name', null, $model) ?>" required>
        <?= html_error_block('name') ?>
    </div>
</div>
<div class="form-group <?= html_error_class('brand') ?>">
    <label class="col-sm-2 control-label">Brand</label>
    <div class="col-sm-10">
        <input type="text" name="brand" class="form-control" placeholder="Brand" value="<?= input('brand', null, $model) ?>" required>
        <?= html_error_block('brand') ?>
    </div>
</div>
<div class="form-group <?= html_error_class('description') ?>">
    <label class="col-sm-2 control-label">Description</label>
    <div class="col-sm-10">
        <textarea name="description" class="form-control" placeholder="Description" rows="4"><?= input('description', null, $model) ?></textarea>
        <?= html_error_block('description') ?>
    </div>
</div>
<div class="form-group <?= html_error_class('price') ?>">
    <label class="col-sm-2 control-label">Price</label>
    <div class="col-sm-10">
        <input type="number" name="price" step="any" class="form-control" placeholder="Price" value="<?= input('price', null, $model) ?>" required>
        <?= html_error_block('price') ?>
    </div>
</div>
