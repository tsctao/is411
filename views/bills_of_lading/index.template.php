<h1><?= $title ?></h1>

<?=
html_breadcrumb([
    'Bill of Lading' => '',
])
?>

<div class="row toolbar">
    <div class="col-md-6">
        <a href="<?= url('/pages/purchase_orders/index.php') ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add Bill of Lading</a>
    </div>
    <div class="col-md-6">
        <form>
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search Bill of Lading" value="<?= query_string('search') ?>">
                <span class="input-group-btn">
                    <a href="<?= current_url(true) ?>" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span></a>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
                </span>
            </div>
        </form>
    </div>
</div>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>ID <?= html_sort('id') ?></th>
        <th>Supplier <?= html_sort('supplier.name') ?></th>
        <th>Created at <?= html_sort('created_at') ?></th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($bills_of_lading as $bill_of_lading): ?>
        <tr>
            <td><?= $bill_of_lading['id'] ?></td>
            <td><a href="<?= url('/pages/suppliers/edit.php', ['id' => $bill_of_lading['supplier_id']]) ?>"><?= $bill_of_lading['supplier_name'] ?></a></td>
            <td><?= $bill_of_lading['created_at'] ?></td>
            <td>
                <a href="<?= url('/pages/bills_of_lading/view.php', ['id' => $bill_of_lading['id']]) ?>" class="btn btn-info"><span class="glyphicon glyphicon-eye-open"></span> View</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?= html_pagination() ?>
