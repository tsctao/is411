<h1><?= $title ?></h1>

<?=
html_breadcrumb([
    'Bill of Lading' => '/pages/bills_of_lading/index.php',
    'Add Bill of Lading' => ''
])
?>

<form class="form-horizontal" method="POST" action="<?= url('/pages/bills_of_lading/create.php') ?>">
    <input type="hidden" name="purchase_order_id" value="<?= query_string('purchase_order_id') ?>">

    <div class="form-group">
        <label class="col-sm-2 control-label">Supplier</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <a href="<?= url('/pages/suppliers/edit.php', ['id' => $supplier['id']]) ?>"><?= $supplier['name'] ?></a>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Items</label>
        <div class="col-sm-10">
            <table id="quotation-items-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th class="col-sm-5">Product</th>
                    <th class="col-sm-2">Quantity</th>
                    <th class="col-sm-2">Unit Price</th>
                    <th class="col-sm-2">Total</th>
                    <th class="col-sm-1">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($quotation_items as $item): ?>
                    <tr>
                        <td>
                            <a href="<?= url('/pages/products/edit.php', ['id' => $item['product_id']]) ?>"><?= array_where($products, 'id', $item['product_id'])['name'] ?></a>
                            <input type="hidden" name="items[<?= $item['id'] ?>][product_id]" value="<?= $item['product_id'] ?>">
                        </td>
                        <td>
                            <div class="form-group-table">
                                <input type="number" name="items[<?= $item['id'] ?>][quantity]" class="form-control calc-quantity" data-index="<?= $item['id'] ?>" required value="<?= $item['quantity'] ?>">
                            </div>
                        </td>
                        <td>
                            <div class="form-group-table">
                                <input type="number" step="any" name="items[<?= $item['id'] ?>][price]" class="form-control calc-price" data-index="<?= $item['id'] ?>" required value="<?= $item['price'] ?>">
                            </div>
                        </td>
                        <td>
                            <p class="form-control-static calc-ext" data-index="<?= $item['id'] ?>"></p>
                        </td>
                        <td>
                            <button type="button" class="di-delete btn btn-danger">Delete</button>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
                <tfoot>
                <tr>
                    <th>Total</th>
                    <th><span class="calc-total-quantity"></span></th>
                    <th></th>
                    <th><span class="calc-total-amount"></span></th>
                    <th></th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
            <a href="<?= url('/pages/bills_of_lading/index.php') ?>" class="btn btn-default">Cancel</a>
        </div>
    </div>
</form>
