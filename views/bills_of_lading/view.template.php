<h1><?= $title ?></h1>

<?=
html_breadcrumb([
    'Bill of Lading' => '/pages/bill_of_ladings/index.php',
    '#' . $bill_of_lading['id'] => ''
])
?>

<form class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-2 control-label">Quotation</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <a href="<?= url('/pages/quotations/view.php', ['id' => $purchase_order['quotation_id']]) ?>">#<?= $purchase_order['quotation_id'] ?></a>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Purchase Order</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <a href="<?= url('/pages/purchase_orders/view.php', ['id' => $bill_of_lading['purchase_order_id']]) ?>">#<?= $bill_of_lading['purchase_order_id'] ?></a>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Supplier</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <a href="<?= url('/pages/suppliers/edit.php', ['id' => $supplier['id']]) ?>"><?= $supplier['name'] ?></a>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Created At</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <?= $bill_of_lading['created_at'] ?>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Items</label>
        <div class="col-sm-10">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th class="col-sm-6">Product</th>
                    <th class="col-sm-2">Quantity</th>
                    <th class="col-sm-2">Unit Price</th>
                    <th class="col-sm-2">Total</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($bill_of_lading_items as $item): ?>
                    <tr>
                        <td>
                            <a href="<?= url('/pages/products/edit.php', ['id' => $item['product_id']]) ?>"><?= array_where($products, 'id', $item['product_id'])['name'] ?></a>
                        </td>
                        <td>
                            <?= $item['quantity'] ?>
                        </td>
                        <td>
                            <?= number_format($item['price'], 2) ?>
                        </td>
                        <td>
                            <?= number_format($item['quantity'] * $item['price'], 2) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
                <tfoot>
                <tr>
                    <th>Total</th>
                    <th><?= $total_quantity ?></th>
                    <th></th>
                    <th><?= number_format($total_amount, 2) ?></th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <a href="<?= url('/pages/bills_of_lading/index.php') ?>" class="btn btn-success">Done</a>
        </div>
    </div>
</form>
