<h1><?= $title ?></h1>

<?=
html_breadcrumb([
    'Purchase Order' => '',
])
?>

<div class="row toolbar">
    <div class="col-md-6">
        <a href="<?= url('/pages/purchase_orders/add.php') ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add Purchase Order</a>
    </div>
    <div class="col-md-6">
        <form>
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search Purchase Order" value="<?= query_string('search') ?>">
                <span class="input-group-btn">
                    <a href="<?= current_url(true) ?>" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span></a>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
                </span>
            </div>
        </form>
    </div>
</div>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>ID <?= html_sort('id') ?></th>
        <th>Supplier <?= html_sort('supplier.name') ?></th>
        <th>Status <?= html_sort('status') ?></th>
        <th>Created at <?= html_sort('created_at') ?></th>
        <th>Updated at <?= html_sort('updated_at') ?></th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($purchase_orders as $purchase_order): ?>
        <tr>
            <td><?= $purchase_order['id'] ?></td>
            <td><a href="<?= url('/pages/suppliers/edit.php', ['id' => $purchase_order['supplier_id']]) ?>"><?= $purchase_order['supplier_name'] ?></a></td>
            <td><span class="status-<?= $purchase_order['status'] ?>"><?= $purchase_order['status'] ?></span></td>
            <td><?= $purchase_order['created_at'] ?></td>
            <td><?= $purchase_order['updated_at'] ?></td>
            <td>
                <a href="<?= url('/pages/purchase_orders/view.php', ['id' => $purchase_order['id']]) ?>" class="btn btn-info"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                <?php if ($purchase_order['status'] == 'open'): ?>
                    <a href="<?= url('/pages/bills_of_lading/add.php', ['purchase_order_id' => $purchase_order['id']]) ?>" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Receive</a>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?= html_pagination() ?>
