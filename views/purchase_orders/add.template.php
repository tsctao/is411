<h1><?= $title ?></h1>

<?=
html_breadcrumb([
    'Purchase Order' => '/pages/purchase_orders/index.php',
    'Add Purchase Order' => ''
])
?>

<form class="form-horizontal" method="POST" action="<?= url('/pages/purchase_orders/create.php') ?>">
    <input type="hidden" name="quotation_id" value="<?= query_string('quotation_id') ?>">

    <div class="form-group">
        <label class="col-sm-2 control-label">Supplier</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <a href="<?= url('/pages/suppliers/edit.php', ['id' => $supplier['id']]) ?>"><?= $supplier['name'] ?></a>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Items</label>
        <div class="col-sm-10">
            <table id="quotation-items-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th class="col-sm-6">Product</th>
                    <th class="col-sm-2">Quantity</th>
                    <th class="col-sm-2">Unit Price</th>
                    <th class="col-sm-2">Total</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($quotation_items as $item): ?>
                    <tr>
                        <td>
                            <a href="<?= url('/pages/products/edit.php', ['id' => $item['product_id']]) ?>"><?= array_where($products, 'id', $item['product_id'])['name'] ?></a>
                        </td>
                        <td>
                            <?= $item['quantity'] ?>
                        </td>
                        <td>
                            <?= $item['price'] ?>
                        </td>
                        <td>
                            <?= number_format($item['quantity'] * $item['price'], 2) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
                <tfoot>
                <tr>
                    <th>Total</th>
                    <th><?= $total_quantity ?></th>
                    <th></th>
                    <th><?= number_format($total_amount, 2) ?></th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
            <a href="<?= url('/pages/purchase_orders/index.php') ?>" class="btn btn-default">Cancel</a>
        </div>
    </div>
</form>
