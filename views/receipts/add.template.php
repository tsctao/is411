<h1><?= $title ?></h1>

<?=
html_breadcrumb([
    'Receipt' => '/pages/receipts/index.php',
    'Add Receipt' => ''
])
?>

<form class="form-horizontal" method="POST" action="<?= url('/pages/receipts/create.php') ?>">
    <input type="hidden" name="sales_order_id" value="<?= query_string('sales_order_id') ?>">

    <div class="form-group">
        <label class="col-sm-2 control-label">Customer</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <a href="<?= url('/pages/customers/edit.php', ['id' => $customer['id']]) ?>"><?= $customer['name'] ?></a>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Items</label>
        <div class="col-sm-10">
            <table id="quotation-items-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th class="col-sm-6">Product</th>
                    <th class="col-sm-2">Quantity</th>
                    <th class="col-sm-2">Price</th>
                    <th class="col-sm-2">Total</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($sales_order_items as $item): ?>
                    <tr>
                        <td>
                            <a href="<?= url('/pages/products/edit.php', ['id' => $item['product_id']]) ?>"><?= array_where($products, 'id', $item['product_id'])['name'] ?></a>
                        </td>
                        <td>
                            <?= $item['quantity'] ?>
                        </td>
                        <td>
                            <?= number_format($item['price'], 2) ?>
                        </td>
                        <td>
                            <?= number_format($item['quantity'] * $item['price'], 2) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
                <tfoot>
                <tr>
                    <th>Total</th>
                    <th><?= $total_quantity ?></th>
                    <th></th>
                    <th><?= number_format($total_amount, 2) ?></th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Amount</label>
        <div class="col-sm-10">
            <input type="number" name="amount" step="any" class="form-control" placeholder="Amount" required min="<?= $total_amount ?>">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Change</label>
        <div class="col-sm-10">
            <p class="form-control-static change-amount"></p>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
            <a href="<?= url('/pages/receipts/index.php') ?>" class="btn btn-default">Cancel</a>
        </div>
    </div>
</form>

<?php ob_start(); ?>
<script>
  $('input[name="amount"]').on('change', function () {
    var change = $(this).val() - <?= $total_amount ?>;
    $('.change-amount').html(change.toFixed(2));
  });
</script>
<?php $javascript = ob_get_clean() ?>
