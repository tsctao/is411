<h1><?= $title ?></h1>

<?=
html_breadcrumb([
    'Receipt' => '',
])
?>

<div class="row toolbar">
    <div class="col-md-6">
        <a href="<?= url('/pages/receipts/add.php') ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add Receipt</a>
    </div>
    <div class="col-md-6">
        <form>
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search Receipt" value="<?= query_string('search') ?>">
                <span class="input-group-btn">
                    <a href="<?= current_url(true) ?>" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span></a>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
                </span>
            </div>
        </form>
    </div>
</div>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>ID <?= html_sort('id') ?></th>
        <th>Customer <?= html_sort('customer.name') ?></th>
        <th>Created at <?= html_sort('created_at') ?></th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($receipts as $receipt): ?>
        <tr>
            <td><?= $receipt['id'] ?></td>
            <td><a href="<?= url('/pages/customer/edit.php', ['id' => $receipt['customer_id']]) ?>"><?= $receipt['customer_name'] ?></a></td>
            <td><?= $receipt['created_at'] ?></td>
            <td>
                <a href="<?= url('/pages/receipts/view.php', ['id' => $receipt['id']]) ?>" class="btn btn-info"><span class="glyphicon glyphicon-eye-open"></span> View</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?= html_pagination() ?>
