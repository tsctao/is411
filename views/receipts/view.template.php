<h1><?= $title ?></h1>

<?=
html_breadcrumb([
    'Receipt' => '/pages/receipts/index.php',
    '#' . $receipt['id'] => ''
])
?>

<form class="form-horizontal">
    <input type="hidden" name="sales_order_id" value="<?= query_string('sales_order_id') ?>">

    <div class="form-group">
        <label class="col-sm-2 control-label">Sales Order</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <a href="<?= url('/pages/sales_orders/view.php', ['id' => $receipt['sales_order_id']]) ?>">#<?= $receipt['sales_order_id'] ?></a>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Customer</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <a href="<?= url('/pages/customers/edit.php', ['id' => $customer['id']]) ?>"><?= $customer['name'] ?></a>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Created At</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <?= $receipt['created_at'] ?>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Items</label>
        <div class="col-sm-10">
            <table id="quotation-items-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th class="col-sm-6">Product</th>
                    <th class="col-sm-2">Quantity</th>
                    <th class="col-sm-2">Price</th>
                    <th class="col-sm-2">Total</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($sales_order_items as $item): ?>
                    <tr>
                        <td>
                            <a href="<?= url('/pages/products/edit.php', ['id' => $item['product_id']]) ?>"><?= array_where($products, 'id', $item['product_id'])['name'] ?></a>
                        </td>
                        <td>
                            <?= $item['quantity'] ?>
                        </td>
                        <td>
                            <?= number_format($item['price'], 2) ?>
                        </td>
                        <td>
                            <?= number_format($item['quantity'] * $item['price'], 2) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
                <tfoot>
                <tr>
                    <th>Total</th>
                    <th><?= $total_quantity ?></th>
                    <th></th>
                    <th><?= number_format($total_amount, 2) ?></th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Amount</label>
        <div class="col-sm-10">
            <p class="form-control-static"><?= number_format($receipt['amount'], 2) ?></p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Change</label>
        <div class="col-sm-10">
            <p class="form-control-static"><?= number_format($receipt['amount'] - $total_amount, 2) ?></p>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <a href="<?= url('/pages/receipts/index.php') ?>" class="btn btn-success">Done</a>
        </div>
    </div>
</form>
