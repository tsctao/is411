<h1><?= $title ?></h1>

<?=
html_breadcrumb([
    'Sales Order' => '/pages/sales_orders/index.php',
    '#' . $sales_order['id'] => ''
])
?>

<form class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-2 control-label">Customer</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <a href="<?= url('/pages/customers/edit.php', ['id' => $customer['id']]) ?>"><?= $customer['name'] ?></a>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Status</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <span class="status-<?= $sales_order['status'] ?>"><?= $sales_order['status'] ?></span>
                <?php if ($sales_order['status'] == 'complete'): ?>
                    <a href="<?= url('/pages/receipts/view.php', ['id' => $receipt['id']]) ?>">(Receipt #<?= $receipt['id'] ?>)</a>
                <?php endif; ?>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Created At</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <?= $sales_order['created_at'] ?>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Updated At</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <?= $sales_order['updated_at'] ?>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Items</label>
        <div class="col-sm-10">
            <table id="quotation-items-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th class="col-sm-6">Product</th>
                    <th class="col-sm-2">Quantity</th>
                    <th class="col-sm-2">Price</th>
                    <th class="col-sm-2">Total</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($sales_order_items as $item): ?>
                    <tr>
                        <td>
                            <a href="<?= url('/pages/products/edit.php', ['id' => $item['product_id']]) ?>"><?= array_where($products, 'id', $item['product_id'])['name'] ?></a>
                        </td>
                        <td>
                            <?= $item['quantity'] ?>
                        </td>
                        <td>
                            <?= number_format($item['price'], 2) ?>
                        </td>
                        <td>
                            <?= number_format($item['quantity'] * $item['price'], 2) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
                <tfoot>
                <tr>
                    <th>Total</th>
                    <th><?= $total_quantity ?></th>
                    <th></th>
                    <th><?= number_format($total_amount, 2) ?></th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <a href="<?= url('/pages/sales_orders/index.php') ?>" class="btn btn-success">Done</a>
        </div>
    </div>
</form>
