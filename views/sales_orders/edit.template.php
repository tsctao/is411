<h1><?= $title ?></h1>

<?=
html_breadcrumb([
    'Sales Order' => '/pages/sales_orders/index.php',
    'Edit Sales Order' => ''
])
?>

<form class="form-horizontal" method="POST" action="<?= url('/pages/sales_orders/update.php', ['id' => $sales_order['id']]) ?>">
    <?php include '_form.template.php' ?>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
            <a href="<?= url('/pages/sales_orders/index.php') ?>" class="btn btn-default">Cancel</a>
        </div>
    </div>
</form>
