<h1><?= $title ?></h1>

<?=
html_breadcrumb([
    'Sales Order' => '',
])
?>

<div class="row toolbar">
    <div class="col-md-6">
        <a href="<?= url('/pages/sales_orders/add.php') ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add Sales Order</a>
    </div>
    <div class="col-md-6">
        <form>
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search Sales Order" value="<?= query_string('search') ?>">
                <span class="input-group-btn">
                    <a href="<?= current_url(true) ?>" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span></a>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
                </span>
            </div>
        </form>
    </div>
</div>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>ID <?= html_sort('id') ?></th>
        <th>Customer <?= html_sort('customer.name') ?></th>
        <th>Status <?= html_sort('status') ?></th>
        <th>Created At <?= html_sort('created_at') ?></th>
        <th>Updated At <?= html_sort('updated_at') ?></th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($sales_orders as $sales_order): ?>
        <tr>
            <td><?= $sales_order['id'] ?></td>
            <td><a href="<?= url('/pages/customers/edit.php', ['id' => $sales_order['customer_id']]) ?>"><?= $sales_order['customer_name'] ?></a></td>
            <td><span class="status-<?= $sales_order['status'] ?>"><?= $sales_order['status'] ?></span></td>
            <td><?= $sales_order['created_at'] ?></td>
            <td><?= $sales_order['updated_at'] ?></td>
            <td>
                <a href="<?= url('/pages/sales_orders/view.php', ['id' => $sales_order['id']]) ?>" class="btn btn-info"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                <?php if ($sales_order['status'] == 'open'): ?>
                    <a href="<?= url('/pages/sales_orders/edit.php', ['id' => $sales_order['id']]) ?>" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
                    <a href="<?= url('/pages/receipts/add.php', ['sales_order_id' => $sales_order['id']]) ?>" class="btn btn-primary"><span class="glyphicon glyphicon-usd"></span> Receipt</a>
                    <form method="POST" action="<?= url('/pages/sales_orders/delete.php', ['id' => $sales_order['id']]) ?>" class="delete-form confirm-delete">
                        <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                    </form>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?= html_pagination() ?>
