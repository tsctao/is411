<div class="form-group <?= html_error_class('customer_id') ?>">
    <label class="col-sm-2 control-label">Customer</label>
    <div class="col-sm-10">
        <select name="customer_id" class="form-control" required>
            <?php if (input('customer_id', null, $sales_order)): ?>
                <option value="<?= $sales_order['customer_id'] ?>"><?= $customer['name'] ?></option>
            <?php endif; ?>
        </select>
        <?= html_error_block('customer_id') ?>
    </div>
</div>

<div class="form-group">
    <div class="form-group-table <?= html_error_class('items') ?>">
        <label class="col-sm-2 control-label">Items</label>
    </div>
    <div class="col-sm-10">
        <div class="form-group-table <?= html_error_class('items') ?>">
            <?= html_error_block('items') ?>
        </div>
        <table id="sales-order-items-table" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th class="col-sm-5">Product</th>
                <th class="col-sm-2">Quantity</th>
                <th class="col-sm-2">Price</th>
                <th class="col-sm-2">Total</th>
                <th class="col-sm-1">Action</th>
            </tr>
            </thead>
            <tbody id="di-container">
            <tr id="di-template" style="display:none;">
                <td>
                    <div class="form-group-table">
                        <select name="items[:index][product_id]" class="form-control select-item" required data-index=":index"></select>
                    </div>
                </td>
                <td>
                    <div class="form-group-table">
                        <input type="number" name="items[:index][quantity]" class="form-control calc-quantity" data-index=":index" required>
                    </div>
                </td>
                <td>
                    <p class="form-control-static calc-price" data-index=":index"></p>
                </td>
                <td>
                    <p class="form-control-static calc-ext" data-index=":index"></p>
                </td>
                <td>
                    <button type="button" class="di-delete btn btn-danger">Delete</button>
                </td>
            </tr>
            <?php foreach($sales_order_items as $item): ?>
                <tr class="di-item">
                    <td>
                        <div class="form-group-table">
                            <select name="items[<?= $item['id'] ?>][product_id]" class="form-control select-item" required data-index="<?= $item['id'] ?>">
                                <option value="<?= $item['product_id'] ?>"><?= array_where($products, 'id', $item['product_id'])['name'] ?> (<?= array_where($products, 'id', $item['product_id'])['quantity'] ?>)</option>
                            </select>
                        </div>
                    </td>
                    <td>
                        <div class="form-group-table">
                            <input type="number" name="items[<?= $item['id'] ?>][quantity]" class="form-control calc-quantity" data-index="<?= $item['id'] ?>" required value="<?= $item['quantity'] ?>">
                        </div>
                    </td>
                    <td>
                        <p class="form-control-static calc-price" data-index="<?= $item['id'] ?>"><?= $item['price'] ?></p>
                    </td>
                    <td>
                        <p class="form-control-static calc-ext" data-index="<?= $item['id'] ?>"></p>
                    </td>
                    <td>
                        <button type="button" class="di-delete btn btn-danger">Delete</button>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
                <th>Total</th>
                <th><span class="calc-total-quantity"></span></th>
                <th></th>
                <th><span class="calc-total-amount"></span></th>
                <th></th>
            </tr>
            <tr>
                <td colspan="5">
                    <button type="button" class="di-add btn btn-warning"><i class="fa fa-plus"></i> Add</button>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>

<?php ob_start(); ?>
<script>
  $('select[name="customer_id"]').select2({
    placeholder: 'Please select customer',
    minimumInputLength: 1,
    ajax: {
      url: '<?= url('/pages/customers/ajax/index.php') ?>',
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          search: params.term
        }
      },
      processResults: function (data) {
        return {
          results: $.map(data, function (item) {
            return {
              id: item.id,
              text: item.name
            }
          })
        };
      },
      cache: true
    }
  });

  $('#sales-order-items-table').dynamicInput({
    afterAdd: function (element) {
      element.show();

      element.find('.select-item').select2({
        minimumInputLength: 1,
        placeholder: 'Please select product',
        ajax: {
          url: '<?= url('/pages/products/ajax/index.php') ?>',
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              search: params.term
            }
          },
          processResults: function (data) {
            return {
              results: $.map(data, function (item) {
                return {
                  id: item.id,
                  text: item.name + ' (' + item.quantity + ')',
                  price: item.price
                }
              })
            };
          },
          cache: true
        }
      });
    },
    afterDelete: function () {
      calc();
    }
  });

  $(document).on('select2:select', function (event) {
    var index = $(event.target).data('index');

    $('.calc-price[data-index="' + index + '"]').html(event.params.data.price);
    $('.calc-quantity[data-index="' + index + '"]').val(1);
    calc();
  });
</script>
<?php $javascript = ob_get_clean() ?>
