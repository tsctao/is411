<h1><?= $title ?></h1>

<?=
html_breadcrumb([
    'Quotation' => '',
])
?>

<div class="row toolbar">
    <div class="col-md-6">
        <a href="<?= url('/pages/quotations/add.php') ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add Quotation</a>
    </div>
    <div class="col-md-6">
        <form>
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search Quotation" value="<?= query_string('search') ?>">
                <span class="input-group-btn">
                    <a href="<?= current_url(true) ?>" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span></a>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
                </span>
            </div>
        </form>
    </div>
</div>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>ID <?= html_sort('id') ?></th>
        <th>Supplier <?= html_sort('supplier.name') ?></th>
        <th>Status <?= html_sort('status') ?></th>
        <th>Created at <?= html_sort('created_at') ?></th>
        <th>Updated at <?= html_sort('updated_at') ?></th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($quotations as $quotation): ?>
        <tr>
            <td><?= $quotation['id'] ?></td>
            <td><a href="<?= url('/pages/suppliers/edit.php', ['id' => $quotation['supplier_id']]) ?>"><?= $quotation['supplier_name'] ?></a></td>
            <td><span class="status-<?= $quotation['status'] ?>"><?= $quotation['status'] ?></span></td>
            <td><?= $quotation['created_at'] ?></td>
            <td><?= $quotation['updated_at'] ?></td>
            <td>
                <a href="<?= url('/pages/quotations/view.php', ['id' => $quotation['id']]) ?>" class="btn btn-info"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                <?php if ($quotation['status'] == 'open'): ?>
                    <a href="<?= url('/pages/purchase_orders/add.php', ['quotation_id' => $quotation['id']]) ?>" class="btn btn-primary"><span class="glyphicon glyphicon-shopping-cart"></span> Place Order</a>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?= html_pagination() ?>
