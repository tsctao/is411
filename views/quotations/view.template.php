<h1><?= $title ?></h1>

<?=
html_breadcrumb([
    'Quotation' => '/pages/quotations/index.php',
    '#' . $quotation['id'] => ''
])
?>

<form class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-2 control-label">Supplier</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <a href="<?= url('/pages/suppliers/edit.php', ['id' => $supplier['id']]) ?>"><?= $supplier['name'] ?></a>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Status</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <span class="status-<?= $quotation['status'] ?>"><?= $quotation['status'] ?></span>
                <?php if ($quotation['status'] == 'complete'): ?>
                    <a href="<?= url('/pages/purchase_orders/view.php', ['id' => $purchase_order['id']]) ?>">(Purchase order #<?= $purchase_order['id'] ?>)</a>
                <?php endif; ?>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Created At</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <?= $quotation['created_at'] ?>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Updated At</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <?= $quotation['updated_at'] ?>
            </p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Items</label>
        <div class="col-sm-10">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th class="col-sm-6">Product</th>
                    <th class="col-sm-2">Quantity</th>
                    <th class="col-sm-2">Unit Price</th>
                    <th class="col-sm-2">Total</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($quotation_items as $item): ?>
                <tr>
                    <td>
                        <a href="<?= url('/pages/products/edit.php', ['id' => $item['product_id']]) ?>"><?= array_where($products, 'id', $item['product_id'])['name'] ?></a>
                    </td>
                    <td>
                        <?= $item['quantity'] ?>
                    </td>
                    <td>
                        <?= number_format($item['price'], 2) ?>
                    </td>
                    <td>
                        <?= number_format($item['quantity'] * $item['price'], 2) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
                <tfoot>
                <tr>
                    <th>Total</th>
                    <th><?= $total_quantity ?></th>
                    <th></th>
                    <th><?= number_format($total_amount, 2) ?></th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <a href="<?= url('/pages/quotations/index.php') ?>" class="btn btn-success">Done</a>
            <?php if ($quotation['status'] == 'open'): ?>
                <a href="<?= url('/pages/purchase_orders/add.php', ['quotation_id' => $quotation['id']]) ?>" class="btn btn-primary"><span class="glyphicon glyphicon-shopping-cart"></span> Place Order</a>
            <?php endif; ?>
        </div>
    </div>
</form>
