<div class="form-group <?= html_error_class('supplier_id') ?>">
    <label class="col-sm-2 control-label">Supplier</label>
    <div class="col-sm-10">
        <select name="supplier_id" class="form-control" required></select>
        <?= html_error_block('supplier_id') ?>
    </div>
</div>

<div class="form-group">
    <div class="form-group-table <?= html_error_class('items') ?>">
        <label class="col-sm-2 control-label">Items</label>
    </div>
    <div class="col-sm-10">
        <div class="form-group-table <?= html_error_class('items') ?>">
            <?= html_error_block('items') ?>
        </div>
        <table id="quotation-items-table" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th class="col-sm-5">Product</th>
                <th class="col-sm-2">Quantity</th>
                <th class="col-sm-2">Unit Price</th>
                <th class="col-sm-2">Total</th>
                <th class="col-sm-1">Action</th>
            </tr>
            </thead>
            <tbody id="di-container">
            <tr id="di-template">
                <td>
                    <div class="form-group-table">
                        <select name="items[:index][product_id]" class="form-control select-item" required></select>
                    </div>
                </td>
                <td>
                    <div class="form-group-table">
                        <input type="number" name="items[:index][quantity]" class="form-control calc-quantity" data-index=":index" required>
                    </div>
                </td>
                <td>
                    <div class="form-group-table">
                        <input type="number" step="any" name="items[:index][price]" class="form-control calc-price" data-index=":index" required>
                    </div>
                </td>
                <td>
                    <p class="form-control-static calc-ext" data-index=":index"></p>
                </td>
                <td>
                    <button type="button" class="di-delete btn btn-danger">Delete</button>
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <th>Total</th>
                <th><span class="calc-total-quantity"></span></th>
                <th></th>
                <th><span class="calc-total-amount"></span></th>
                <th></th>
            </tr>
            <tr>
                <td colspan="5">
                    <button type="button" class="di-add btn btn-warning"><i class="fa fa-plus"></i> Add</button>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>

<?php ob_start(); ?>
<script>
    $('select[name="supplier_id"]').select2({
      placeholder: 'Please select supplier',
      minimumInputLength: 1,
      ajax: {
        url: '<?= url('/pages/suppliers/ajax/index.php') ?>',
        dataType: 'json',
        delay: 250,
        data: function (params) {
          return {
            search: params.term
          }
        },
        processResults: function (data) {
          return {
            results: $.map(data, function (item) {
              return {
                id: item.id,
                text: item.name
              }
            })
          };
        },
        cache: true
      }
    });

    $('#quotation-items-table').dynamicInput({
      afterAdd: function (element) {
        element.find('.select-item').select2({
          minimumInputLength: 1,
          placeholder: 'Please select product',
          ajax: {
            url: '<?= url('/pages/products/ajax/index.php') ?>',
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                search: params.term
              }
            },
            processResults: function (data) {
              return {
                results: $.map(data, function (item) {
                  return {
                    id: item.id,
                    text: item.name
                  }
                })
              };
            },
            cache: true
          }
        });
      },
      afterDelete: function () {
        calc();
      }
    });
</script>
<?php $javascript = ob_get_clean() ?>
