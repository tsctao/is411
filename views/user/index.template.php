<h1><?= $title ?></h1>

 <?=
html_breadcrumb([
    'User' => '',
])
?>

<div class="row toolbar">
    <div class="col-md-6">
        <a href="<?= url('/pages/user/add.php') ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add User</a>
    </div>
    <div class="col-md-6">
        <form>
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search User" value="<?= query_string('search') ?>">
                <span class="input-group-btn">
                    <a href="<?= current_url(true) ?>" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span></a>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
                </span>
            </div>
        </form>
    </div>
</div>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Name <?= html_sort('name') ?></th>
        <th>Address <?= html_sort('address') ?></th>
        <th>Username <?= html_sort('username') ?></th>
        <th>Role<?= html_sort('role') ?></th>
		<th>Create At<?= html_sort('created_at') ?></th>
		<th>Update At<?= html_sort('updated_at') ?></th>
		<th>Action</th>
		
    </tr>
    </thead>
    <tbody>
    <?php foreach ($users as $user): ?>
        <tr>
            <td><?= $user['name'] ?></td>
            <td><?= $user['address'] ?></td>
            <td><?= $user['username'] ?></td>       
			<td><?= $user['role'] ?></td>
			<td><?= $user['created_at'] ?></td>
			<td><?= $user['updated_at'] ?></td>
			
            <td>
                <a href="<?= url('/pages/user/edit.php', ['id' => $user['id']]) ?>" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
                <form method="POST" action="<?= url('/pages/user/delete.php', ['id' => $user['id']]) ?>" class="delete-form confirm-delete">
                    <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?= html_pagination() ?>
