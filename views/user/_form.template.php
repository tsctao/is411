<div class="form-group <?= html_error_class('name') ?>">
    <label class="col-sm-2 control-label">Name</label>
    <div class="col-sm-10">
        <input type="text" name="name" class="form-control" placeholder="Name" value="<?= input('name', null, $model) ?>">
        <?= html_error_block('name') ?>
    </div>
</div>
<div class="form-group <?= html_error_class('address') ?>">
    <label class="col-sm-2 control-label">Address</label>
    <div class="col-sm-10">
        <input type="text" name="address" class="form-control" placeholder="address" value="<?= input('address', null, $model) ?>">
        <?= html_error_block('address') ?>
    </div>
</div>
 <div class="form-group <?= html_error_class('username') ?>">
    <label class="col-sm-2 control-label">Username</label>
    <div class="col-sm-10">
        <input type="text" name="username" class="form-control" pattern="[a-z\d\.]{4,}"
        title="Only lowercase letters and numbers; at least 4 characters e.g. user1" placeholder="username" value="<?= input('username', null, $model) ?>">
        <?= html_error_block('username') ?>
    </div>
</div>

<div class="form-group <?= html_error_class('password') ?>">
    <label class="col-sm-2 control-label">Password</label>
    <div class="col-sm-10">
        <input type="password" name="password" class="form-control" placeholder="password">
        <?= html_error_block('password') ?>
    </div>
</div>
<div class="form-group <?= html_error_class('role') ?>">
    <label class="col-sm-2 control-label">Role</label>
    <div class="col-sm-10">
        <select name="role" class="form-control">
            <option value="admin" <?= input('role', null, $model) == 'admin' ? 'selected' : '' ?>>Admin</option>
            <option value="user" <?= input('role', null, $model) == 'user' ? 'selected' : '' ?>>User</option>
        </select>
        <?= html_error_block('role') ?>
    </div>
</div>
 
 