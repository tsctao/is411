<h1><?= $title ?></h1>

<?=
    html_breadcrumb([
        'Supplier' => '',
    ])
?>

<div class="row toolbar">
    <div class="col-md-6">
        <a href="<?= url('/pages/suppliers/add.php') ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add Supplier</a>
    </div>
    <div class="col-md-6">
        <form>
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search Supplier" value="<?= query_string('search') ?>">
                <span class="input-group-btn">
                    <a href="<?= current_url(true) ?>" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span></a>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
                </span>
            </div>
        </form>
    </div>
</div>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Name <?= html_sort('name') ?></th>
        <th>Address <?= html_sort('address') ?></th>
        <th>Email <?= html_sort('email') ?></th>
        <th>Telephone <?= html_sort('telephone') ?></th>
        <th>Updated at <?= html_sort('updated_at') ?></th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($suppliers as $supplier): ?>
        <tr>
            <td><?= $supplier['name'] ?></td>
            <td><?= $supplier['address'] ?></td>
            <td><?= $supplier['email'] ?></td>
            <td><?= $supplier['telephone'] ?></td>
            <td><?= $supplier['updated_at'] ?></td>
            <td>
                <a href="<?= url('/pages/suppliers/edit.php', ['id' => $supplier['id']]) ?>" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
                <form method="POST" action="<?= url('/pages/suppliers/delete.php', ['id' => $supplier['id']]) ?>" class="delete-form confirm-delete">
                    <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?= html_pagination() ?>
