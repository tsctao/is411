<h1><?= $title ?></h1>

<?=
html_breadcrumb([
    'Supplier' => '/pages/suppliers/index.php',
    'Add Supplier' => ''
])
?>

<form class="form-horizontal" method="POST" action="<?= url('/pages/suppliers/create.php') ?>">
    <?php include '_form.template.php' ?>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
            <a href="<?= url('/pages/suppliers/index.php') ?>" class="btn btn-default">Cancel</a>
        </div>
    </div>
</form>
