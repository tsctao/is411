<div class="form-group <?= html_error_class('name') ?>">
    <label class="col-sm-2 control-label">Name</label>
    <div class="col-sm-10">
        <input type="text" name="name" class="form-control" placeholder="Name" value="<?= input('name', null, $model) ?>">
        <?= html_error_block('name') ?>
    </div>
</div>
<div class="form-group <?= html_error_class('address') ?>">
    <label class="col-sm-2 control-label">Address</label>
    <div class="col-sm-10">
      <textarea name="address" class="form-control" placeholder="Address" rows="4"><?= input('address', null, $model) ?></textarea>
        <?= html_error_block('address') ?>
    </div>
</div>
<div class="form-group <?= html_error_class('email') ?>">
    <label class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
      <input type="email" name="email" step="any" class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="Please enter a valid email address" placeholder="Email" value="<?= input('email', null, $model) ?>">
        <?= html_error_block('email') ?>
    </div>
</div>
<div class="form-group <?= html_error_class('telephone') ?>">
    <label class="col-sm-2 control-label">Telephone </label>
    <div class="col-sm-10">
        <input type="text" name="telephone" step="any" pattern="[0-9].{8,}"  class="form-control" placeholder="Telephone" title="Please enter a valid telephone number" value="<?= input('telephone', null, $model) ?>">
        <?= html_error_block('telephone') ?>
    </div>
</div>
