<?php

$database = mysqli_connect($config['db_host'], $config['db_user'], $config['db_pass'], $config['db_name']);

if (! $database) {
    die('Database connection failed: ' . mysqli_connect_error());
}

mysqli_set_charset($database, 'utf8mb4');
