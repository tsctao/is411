<?php

if (! function_exists('html_breadcrumb')) {
    /**
     * Generate breadcrumb
     *
     * @param array $pages
     * @return string
     */
    function html_breadcrumb(array $pages) {
        $html = '';

        $html .= '<ol class="breadcrumb">';
        $html .= '<li><a href="' . url() . '">Home</a></li>';
        foreach ($pages as $name => $url) {
            if (!empty($url)) {
                $html .= '<li><a href="' . url($url) . '">' . $name . '</a></li>';
            } else {
                $html .= '<li class="active">' . $name . '</li>';
            }
        }
        $html .= '</ol>';

        return $html;
    }
}

if (! function_exists('html_sort')) {
    /**
     * Generate sort column button
     *
     * @param $column
     * @return string
     */
    function html_sort($column) {
        $html = '';

        $sort = query_string('sort');
        if (substr($sort, 0, 1) == '-') {
            $sort = substr($sort, 1);
            $inverse = true;
        } else {
            $inverse = false;
        }

        if ($sort == $column) {
            $html .= '<a href="' . current_url('page', ['sort' => $inverse ? $column : '-' . $column]) . '">';
            $html .= '<span class="glyphicon glyphicon-sort-by-attributes' . ($inverse ? '-alt' : '') . '"></span>';
            $html .= '</a>';
        } else {
            $html .= '<a href="' . current_url('page', ['sort' => $column]) . '">';
            $html .= '<span class="glyphicon glyphicon-sort"></span>';
            $html .= '</a>';
        }

        return $html;
    }
}

if (! function_exists('html_pagination')) {
    /**
     * Generate pagination button.
     *
     * @return string
     */
    function html_pagination() {
        $html = '';
        $current_page = query_string('page', 1);

        $html .= '<nav>';
        $html .= '<ul class="pagination">';
        for ($page = 1; $page <= $GLOBALS['total_pages']; $page++) {
            $html .= '<li class="' . ($page == $current_page ? 'active' : '') . '">';
            $html .= '<a href="' . current_url(false, ['page' => $page]) . '">' . $page . '</a>';
            $html .= '</li>';
        }
        $html .= '</ul>';
        $html .= '</nav>';

        return $html;
    }
}

if (! function_exists('html_error_class')) {
    /**
     * Generate has-error class.
     *
     * @param $field
     * @return string
     */
    function html_error_class($field) {
        return validation_error($field) ? 'has-error' : '';
    }
}

if (! function_exists('html_error_block')) {
    /**
     * Generate input error block.
     *
     * @param $field
     * @return string
     */
    function html_error_block($field) {
        $html = '';

        $error = validation_error($field);

        if (!is_null($error)) {
            $html .= '<span class="help-block">' . $error . '</span>';
        }

        return $html;
    }
}
