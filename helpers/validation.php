<?php

if (! function_exists('validate')) {
    /**
     * Validate data with given rules.
     *
     * @param array $rules
     * @param array $data
     * @return bool
     */
    function validate(array $rules, array $data) {
        $errors = [];

        foreach ($rules as $field => $rule) {
            foreach ($rule as $key => $value) {
                $inputs = [];
                if (strpos($field, '[]') !== false) {
                    $field = explode('[]', $field);
                    $parent = $field[0];
                    $child = substr($field[1], 1, -1);

                    if (isset($data[$parent])) {
                        foreach ($data[$parent] as $k => $v) {
                            $inputs["{$parent}[{$k}][$child]"] = $v[$child];
                        }
                    }
                } else {
                    $inputs[$field] = isset($data[$field]) ? $data[$field] : null;
                }

                if (is_string($key)) {
                    $parameter = $value;
                    $value = $key;
                } else {
                    $parameter = null;
                }

                foreach ($inputs as $field => $input) {
                    $parameters = [$input];

                    if (!is_null($parameter)) {
                        $parameters[] = $parameter;
                    }

                    $error = call_user_func('validate_' . $value, ...$parameters);
                    if (!is_null($error)) {
                        $errors[$field][] = $error;
                    }
                }

            }
        }

        if (count($errors)) {
            $_SESSION['errors'] = $errors;
            return false;
        }

        return true;
    }
}

if (! function_exists('validation_error')) {
    /**
     * Retrieve validation errors.
     *
     * @param null $field
     * @return array|null
     */
    function validation_error($field = null) {
        $errors = isset($_SESSION['errors']) ? $_SESSION['errors'] : [];

        if (is_null($field)) {
            return $errors;
        }

        if (isset($errors[$field])) {
            return $errors[$field][0];
        }

        return null;
    }
}

if (! function_exists('validate_required')) {
    /**
     * Validate that value exists.
     *
     * @param $value
     * @return null|string
     */
    function validate_required($value) {
        $message = 'This field is required.';

        if (is_null($value)) {
            return $message;
        } elseif (is_string($value) && trim($value) === '') {
            return $message;
        } elseif (is_array($value) && count($value) < 1) {
            return $message;
        }

        return null;
    }
}

if (! function_exists('validate_decimal')) {
    /**
     * Validate that value is decimal.
     *
     * @param $value
     * @return null|string
     */
    function validate_decimal($value) {
        $message = 'This field must be decimal.';

        if (!preg_match('/^[\-+]?[0-9]+(\.[0-9]+)?$/', $value)) {
            return $message;
        }

        return null;
    }
}

if (! function_exists('validate_pattern')) {
    /**
     * Validate that value match given pattern.
     *
     * @param $value
     * @param $pattern
     * @return null|string
     */
    function validate_pattern($value, $pattern) {
        $message = 'This field format is incorrect.';

        if (!preg_match($pattern, $value)) {
            return $message;
        }

        return null;
    }
}
