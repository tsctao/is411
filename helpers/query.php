<?php

if (! function_exists('query_list')) {
    /**
     * Get items from the table.
     *
     * Options:
     * searchable: define columns array that can be search.
     * sortable: define columns array that can be sort.
     * default_sort: default sort column.
     * default_direction: default sort direction.
     * pagination: the number of item per page.
     * dates: define date columns
     *
     * @param $database
     * @param $table
     * @param array $options
     * @return array
     */
    function query_list($database, $table, $options = []) {
        $result = [];
        $dates = [];
        $select = "SELECT `{$table}`.*";
        $joins = '';

        if (isset($options['join'])) {
            foreach ($options['join'] as $join) {
                if (strpos($join[2], '.') === false) {
                    $column = "`{$table}`.`{$join[2]}`";
                }

                $joins .= " LEFT JOIN `{$join[0]}` ON {$join[2]} = `{$join[0]}`.`{$join[1]}`";

                foreach ($join[3] as $column) {
                    $select .= ", `{$join[0]}`.`{$column}` AS {$join[0]}_{$column}";
                }
            }
        }

        $sql = "{$select} FROM `{$table}`{$joins}";

        $search_count = 0;

        if (isset($options['searchable'])) {
            $search = query_string('search');

            if (!is_null($search)) {
                foreach ($options['searchable'] as $column) {
                    if (isset($options['dates']) && in_array($column, $options['dates'])) {
                        $dates[$column] = $search_count;
                    }

                    if (strpos($column, '.') === false) {
                        $column = "`{$table}`.`{$column}`";
                    }

                    $sql .= ($search_count ? ' OR' : ' WHERE') . " {$column} LIKE ?";

                    $search_count++;
                }
            }
        }

        if (isset($options['sortable']) || isset($options['default_sort']) || isset($options['default_direction'])) {
            $sort = isset($options['default_sort']) ? $options['default_sort'] : 'id';
            $direction = isset($options['default_direction']) ? $options['default_direction'] : 'asc';
            $current_sort = query_string('sort');

            if (!is_null($current_sort)) {
                if (substr($current_sort, 0, 1) == '-') {
                    $current_direction = 'desc';
                    $current_sort = substr($current_sort, 1);
                } else {
                    $current_direction = 'asc';
                }

                if (in_array($current_sort, $options['sortable'])) {
                    $sort = $current_sort;
                    $direction = $current_direction;
                }
            }

            if (strpos($sort, '.') === false) {
                $sort = "`{$table}`.`{$sort}`";
            }

            $sql .= " ORDER BY {$sort} {$direction}";
        }

        $stmt = mysqli_prepare($database, $sql);

        if (!$stmt) {
            die('Database prepare failed: ' . mysqli_error($database));
        }

        if ($search_count) {
            $filled = array_fill(0, $search_count, '%' . query_string('search') . '%');

            foreach ($dates as $column => $index) {
                $keyword = substr($filled[$index], 1, -1);

                if (preg_match('/[^0-9\-:]+/', $keyword)) {
                    $filled[$index] = '';
                }
            }

            mysqli_stmt_bind_param($stmt, str_repeat('s', $search_count), ...$filled);
        }

        if (!mysqli_stmt_execute($stmt)) {
            die('Database execute failed: ' . mysqli_stmt_error($stmt));
        };

        if (isset($options['pagination'])) {
            $result_set = mysqli_stmt_get_result($stmt);
            $total = mysqli_num_rows($result_set);

            mysqli_free_result($result_set);
            mysqli_stmt_close($stmt);

            $current_page = query_string('page', 1);
            $limit = $options['pagination'];
            $offset = $limit * ($current_page - 1);
            $GLOBALS['total_pages'] = ceil($total / $limit);

            $sql .= " LIMIT {$limit} OFFSET {$offset}";

            $stmt = mysqli_prepare($database, $sql);

            if (!$stmt) {
                die('Database prepare failed: ' . mysqli_error($database));
            }

            if ($search_count) {
                $filled = array_fill(0, $search_count, '%' . query_string('search') . '%');

                foreach ($dates as $column => $index) {
                    $keyword = substr($filled[$index], 1, -1);

                    if (preg_match('/[^0-9\-:]+/', $keyword)) {
                        $filled[$index] = '';
                    }
                }

                mysqli_stmt_bind_param($stmt, str_repeat('s', $search_count), ...$filled);
            }

            if (!mysqli_stmt_execute($stmt)) {
                die('Database execute failed: ' . mysqli_stmt_error($stmt));
            };
        }

        $result_set = mysqli_stmt_get_result($stmt);

        while ($row = mysqli_fetch_assoc($result_set)) {
            $result[] = $row;
        }

        mysqli_free_result($result_set);
        mysqli_stmt_close($stmt);

        return $result;
    }
}

if (! function_exists('query_find')) {
    /**
     * Get specified item from the table.
     *
     * @param $database
     * @param $table
     * @param $id
     * @param string $column
     * @param array $options
     * @return array|null
     */
    function query_find($database, $table, $id, $column = 'id', $options = []) {
        $sql = "SELECT * FROM `{$table}`";

        if (is_array($id)) {
            $in = implode(', ', array_fill(0, count($id), '?'));
            $sql .= " WHERE `{$column}` IN ({$in})";
        } else {
            $sql .= " WHERE `{$column}` = ?";
        }

        $stmt = mysqli_prepare($database, $sql);

        if (!$stmt) {
            die('Database prepare failed: ' . mysqli_error($database));
        }

        if (is_array($id)) {
            mysqli_stmt_bind_param($stmt, str_repeat('s', count($id)), ...$id);
        } else {
            mysqli_stmt_bind_param($stmt, 's', $id);
        }

        if (!mysqli_stmt_execute($stmt)) {
            die('Database execute failed: ' . mysqli_stmt_error($stmt));
        };

        $result_set = mysqli_stmt_get_result($stmt);

        if (isset($options['list']) && $options['list']) {
            $result = [];

            while ($row = mysqli_fetch_assoc($result_set)) {
                $result[] = $row;
            }
        } else {
            $result = mysqli_fetch_assoc($result_set);
        }

        mysqli_free_result($result_set);
        mysqli_stmt_close($stmt);

        return $result;
    }
}

if (! function_exists('query_insert')) {
    /**
     * Insert an item to the table.
     *
     * Options:
     * created_timestamp: define created timestamp column (set to false for disable).
     * updated_timestamp: define updated timestamp column (set to false for disable).
     *
     * @param $database
     * @param $table
     * @param array $data
     * @param array $options
     * @return int|string
     */
    function query_insert($database, $table, array $data, $options = []) {
        $sql = "INSERT INTO `{$table}`";

        $created_timestamp = isset($options['created_timestamp']) ? $options['created_timestamp'] : 'created_at';
        $updated_timestamp = isset($options['updated_timestamp']) ? $options['updated_timestamp'] : 'updated_at';

        $timestamp = date('Y-m-d H:i:s');

        if ($created_timestamp) {
            $data[$created_timestamp] = $timestamp;
        }

        if ($updated_timestamp) {
            $data[$updated_timestamp] = $timestamp;
        }

        $columns = array_map(function ($column) {
            return "`{$column}`";
        }, array_keys($data));

        $sql .= ' (' . implode(', ', $columns) . ')';
        $sql .= ' VALUES (' . implode(', ', array_fill(0, count($data), '?')) . ')';

        $stmt = mysqli_prepare($database, $sql);

        if (!$stmt) {
            die('Database prepare failed: ' . mysqli_error($database));
        }

        mysqli_stmt_bind_param($stmt, str_repeat('s', count($data)), ...array_values($data));

        if (!mysqli_stmt_execute($stmt)) {
            die('Database execute failed: ' . mysqli_stmt_error($stmt));
        };

        if (mysqli_stmt_affected_rows($stmt) < 1) {
            die('Database execute failed: No affected row.');
        }

        mysqli_stmt_close($stmt);

        return mysqli_insert_id($database);
    }
}

if (! function_exists('query_update')) {
    /**
     * Update an item in the table;
     *
     * Options:
     * updated_timestamp: define updated timestamp column (set to false for disable).
     *
     * @param $database
     * @param $table
     * @param $id
     * @param array $data
     * @param array $options
     */
    function query_update($database, $table, $id, array $data, $options = []) {
        $sql = "UPDATE `{$table}`";

        $updated_timestamp = isset($options['updated_timestamp']) ? $options['updated_timestamp'] : 'updated_at';

        if ($updated_timestamp) {
            $data[$updated_timestamp] = date('Y-m-d H:i:s');
        }

        $set = array_map(function ($column) {
            return "`{$column}` = ?";
        }, array_keys($data));

        $sql .= ' SET ' . implode(', ', $set);

        $id_column = isset($options['primary_key']) ? $options['primary_key'] : 'id';

        $sql .= " WHERE `{$id_column}` = ?";
        $data[] = $id;

        $stmt = mysqli_prepare($database, $sql);

        if (!$stmt) {
            die('Database prepare failed: ' . mysqli_error($database));
        }

        mysqli_stmt_bind_param($stmt, str_repeat('s', count($data)), ...array_values($data));

        if (!mysqli_stmt_execute($stmt)) {
            die('Database execute failed: ' . mysqli_stmt_error($stmt));
        };

        if (mysqli_stmt_affected_rows($stmt) < 1) {
            die('Database execute failed: No affected row.');
        }

        mysqli_stmt_close($stmt);
    }
}

if (! function_exists('query_delete')) {
    /**
     * Delete an item in the table;
     *
     * @param $database
     * @param $table
     * @param $id
     * @param string $column
     */
    function query_delete($database, $table, $id, $column = 'id') {
        $sql = "DELETE FROM `{$table}` WHERE `{$column}` = ?";

        $stmt = mysqli_prepare($database, $sql);

        if (!$stmt) {
            die('Database prepare failed: ' . mysqli_error($database));
        }

        mysqli_stmt_bind_param($stmt, 's', $id);

        if (!mysqli_stmt_execute($stmt)) {
            die('Database execute failed: ' . mysqli_stmt_error($stmt));
        };

        if (mysqli_stmt_affected_rows($stmt) < 1) {
            die('Database execute failed: No affected row.');
        }

        mysqli_stmt_close($stmt);
    }
}
