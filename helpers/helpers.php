<?php

if (! function_exists('dd')) {
    /**
     * Dump variable and die.
     *
     * @param $variable
     */
    function dd($variable) {
        echo '<pre>';
        var_dump($variable);
        echo '</pre>';

        die();
    }
}

if (! function_exists('query_string')) {
    /**
     * Return query string.
     *
     * @param string|null $key
     * @param mixed $default
     * @return array|string
     */
    function query_string($key = null, $default = null) {
        $query = $_GET;

        if (is_null($key)) {
            return $query;
        }

        return isset($query[$key]) ? $query[$key] : $default;
    }
}

if (! function_exists('current_url')) {
    /**
     * Return current url except or include specified query strings.
     *
     * @param mixed $except
     * @param array $include
     * @return string
     */
    function current_url($except = false, $include = []) {
        $url = 'http://' . $_SERVER['SERVER_NAME'];
        if ($_SERVER['SERVER_PORT'] != 80) {
            $url .= ':' . $_SERVER['SERVER_PORT'];
        }
        $url .= explode('?', $_SERVER['REQUEST_URI'])[0];

        $query = $_GET;

        if ($except !== false) {
            if ($except === true) {
                $query = [];
            } else {
                $except = is_array($except) ? $except : [$except];

                foreach ($except as $key) {
                    unset($query[$key]);
                }
            }
        }

        $query = $include + $query;

        return $url . (count($query) ? '?' . http_build_query($query) : '');
    }
}

if (! function_exists('url')) {
    /**
     * Return an absolute URL of the path.
     *
     * @param string|null $path
     * @param array $query
     * @return string
     */
    function url($path = null, $query = []) {
        $file_path = $_SERVER['SCRIPT_FILENAME'];
        $request = str_replace(BASE_PATH, '', $file_path);
        $full_url = current_url(true);

        return str_replace($request, '', $full_url) . $path . (count($query) ? '?' . http_build_query($query) : '');
    }
}

if (! function_exists('redirect')) {
    /**
     * Redirect to the path.
     *
     * @param $path
     * @param array $query
     */
    function redirect($path, $query = []) {
        header('location: ' . url($path, $query));
        $_SESSION['redirect'] = true;
        exit();
    }
}

if (! function_exists('input')) {
    /**
     * Return an old value of input.
     *
     * @param $name
     * @param mixed $default
     * @param array $model
     * @return mixed|null
     */
    function input($name, $default = null, $model = null) {
        $old_input = $_SESSION['old_input'];

        if (isset($old_input[$name])) {
            return $old_input[$name];
        }

        if (is_array($model) && isset($model[$name])) {
            return $model[$name];
        }

        return $default;
    }
}

if (! function_exists('array_where')) {
    /**
     * Return specified item from the array.
     *
     * @param array $array
     * @param $column
     * @param $value
     * @return mixed|null
     */
    function array_where(array $array, $column, $value) {
        foreach ($array as $item) {
            if ($item[$column] == $value) {
                return $item;
            }
        }

        return null;
    }
}
