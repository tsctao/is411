<?php

if (! function_exists('auth_set')) {
    function auth_set($value) {
        $_SESSION['user'] = $value;
    }
}

if (! function_exists('auth_unset')) {
    function auth_unset() {
        unset($_SESSION['user']);
    }
}

if (! function_exists('auth_is_login')) {
    function auth_is_login($auth) {
        return !is_null($auth);
    }
}

if (! function_exists('auth_check')) {
    function auth_check($auth, $role, $redirect = false) {
        if (is_null($auth)) {
            if ($redirect) {
                redirect('/pages/auth/login.php');
            }

            return false;
        }

        if (!is_array($role)) {
            $role = [$role];
        }

        if (!in_array($auth['role'], $role)) {
            if ($redirect) {
                redirect('/pages/auth/login.php');
            }

            return false;
        }

        return true;
    }
}
