<?php

if (! function_exists('flash_set')) {
    /**
     * Save flash message into session.
     *
     * @param $value
     * @param string $key
     */
    function flash_set($value, $key = 'flash') {
        $_SESSION[$key] = $value;
    }
}

if (! function_exists('flash_get')) {
    /**
     * Retrieve flash message from session.
     *
     * @param string $key
     * @return mixed
     */
    function flash_get($key = 'flash') {
        if (!isset($_SESSION[$key])) {
            return null;
        }

        $value = $_SESSION[$key];
        unset($_SESSION[$key]);

        return $value;
    }
}
